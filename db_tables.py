from peewee import *

db = SqliteDatabase(None)

class BaseModel(Model):
    id = AutoField(primary_key=True)
    class Meta:
        database = db

class User(BaseModel):
    username = CharField(unique=True)
    password = CharField()

class Permission(BaseModel):
    name = CharField(unique=True)

class UserPermission(BaseModel):
    user = ForeignKeyField(User, to_field='id')
    permission = ForeignKeyField(Permission, to_field='id')

class Creature(BaseModel):
    name = CharField()
    hp = IntegerField()
    strength = IntegerField()
    dexterity = IntegerField()
    constitution = IntegerField()
    intelligence = IntegerField()
    wisdom = IntegerField()
    charisma = IntegerField()
    level = IntegerField()
    exp = IntegerField()

class PlayerCharacter(BaseModel):
    user_id = ForeignKeyField(User, to_field='id')
    name = CharField()
    hp = IntegerField()
    strength = IntegerField()
    dexterity = IntegerField()
    constitution = IntegerField()
    intelligence = IntegerField()
    wisdom = IntegerField()
    charisma = IntegerField()
    level = IntegerField()
    exp = IntegerField()

class Item(BaseModel):
    name = CharField()
    item_type = CharField()

class Armor(BaseModel):
    item_id = ForeignKeyField(Item, to_field='id')
    armor_value = IntegerField()
    max_dex = IntegerField()

class Weapon(BaseModel):
    item_id = ForeignKeyField(Item, to_field='id')
    dice_count = IntegerField()
    dice_size = IntegerField()
    modifier = IntegerField()

class Consumeable(BaseModel):
    item_id = ForeignKeyField(Item, to_field='id')
    beneficial = BooleanField()
    dice_size = IntegerField()
    dice_count = IntegerField()

class InventoryItem(BaseModel):
    character_id = ForeignKeyField(PlayerCharacter, to_field='id')
    item_id = ForeignKeyField(Item, to_field='id')
    amount = IntegerField()

class PotionSlot(BaseModel):
    character_id = ForeignKeyField(PlayerCharacter, to_field='id')
    item_id = ForeignKeyField(Consumeable, to_field='id')
    amount = IntegerField()

