import os
import json
from peewee import *
from db_tables import User, Permission, UserPermission, db


class LoginDataController:
    def __init__(self, db_path: str):
        self._db = db
        self._db.init(db_path)
        self._create_tables()
        self._initialize_permissions()
        self._initialize_admin_user()

    def __del__(self):
        self._db.close()

    def _create_tables(self):
        self._db.create_tables([User, Permission, UserPermission])

    def _initialize_permissions(self):
        predefined_permissions = ['user', 'moderator', 'admin']
        for permission in predefined_permissions:
            Permission.get_or_create(name=permission)

    def _initialize_admin_user(self):
        admin_username = 'admin'
        admin_password = 'changeme'
        admin_permissions = ['admin']

        user = User.select().where(User.username == admin_username)
        if not user:
            User.create(username=admin_username, password=admin_password)
            admin = User.get(username=admin_username)
            for permission in admin_permissions:
                permission_obj = Permission.get(name=permission)
                UserPermission.get_or_create(user=admin.id, permission=permission_obj.id)
    
    def validate_user_credentials(self, username: str, password: str) -> bool:
        user = User.select().where(User.username == username)
        if user:
            return user[0].password == password
        return False

    def add_new_account(self, new_username: str, new_password: str, confirm_password: str) -> tuple[bool, str]:        
        exists = User.select().where(User.username == new_username)
        if exists:
            return False, 'Username already in use!'
            
        if new_password == confirm_password:
            User.create(username=new_username, password=new_password)
            user = User.get(username=new_username)
            permission_obj = Permission.get(name='user')
            UserPermission.create(user=user.id, permission=permission_obj.id)
            return True, ''
        else:
            return False, 'Passwords do not match!'

    def change_password(self, username: str, new_password: str, confirm_password: str) -> tuple[bool, str]:
        if new_password == confirm_password:
            user = User.select().where(User.username == username)
            if not user:
                return False, 'User not found!'
            user[0].password = new_password
            user[0].save()
            return True, 'Password changed successfully!'
        else:
            return False, 'Passwords do not match!'

    def user_has_permission(self, username: str, permission: str) -> bool:
        user = User.select().where(User.username == username)
        if not user:
            return False
        user = user[0]
        permission_obj = Permission.get(Permission.name == permission)
        return UserPermission.select().where(UserPermission.user == user.id, UserPermission.permission == permission_obj.id).exists()

    def get_usernames(self):
        return [user.username for user in User.select()]
            

    def reset_user_password(self, admin_username: str, admin_password: str, new_password: str, confirm_password: str, username: str) -> bool:
        if self.validate_user_credentials(admin_username, admin_password):
            user = User.get(User.username == username)
            if not user:
                return False
            if new_password == confirm_password:
                user.password = new_password
                user.save()
                return True
        return False

    def _add_permission(self, username: str, permission: str) -> bool:
        user = User.select().where(User.username == username)
        permission_obj = Permission.get(name=permission)
        if not user or not permission_obj:
            return False
        UserPermission.get_or_create(user=user[0].id, permission=permission_obj.id)
        return True

    def _remove_permission(self, username: str, permission: str) -> bool:
        user = User.select().where(User.username == username)
        permission_obj = Permission.get(Permission.name == permission)
        if not user or not permission_obj:
            return False
        UserPermission.delete().where(UserPermission.user == user[0].id).where(UserPermission.permission == permission_obj.id)
        return True

    def set_user_permissions(self, admin_username: str, admin_password: str, action: str, permission: str, username: str) -> bool:
        if self.validate_user_credentials(admin_username, admin_password):
            if action == 'add':
                return self._add_permission(username, permission)
            elif action == 'remove':
                return self._remove_permission(username, permission)
        return False
